﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PluralSightPackageInstaller;

namespace PluralSightPackageInstallerTests
{
	[TestClass]
	public class ProgramUnitTests
	{
		#region HandleArgs
		[TestMethod]
		public void HandleArgs_CommandLineArgs_NoArgs()
		{
			//One line string arg
			string[] args = { };
			Assert.AreEqual(0, args.Length);

			bool result = Program.HandleArgs(args);
			Assert.AreEqual(false, result);
		}

		[TestMethod]
		public void HandleArgs_CommandLineArgs_OneLine()
		{
			//One line string arg
			string[] args = { "[ KittenService: CamelCaser, CamelCaser:  ]" };
			Assert.AreEqual(1, args.Length);

			bool result = Program.HandleArgs(args);
			Assert.AreEqual(true, result);
		}

		[TestMethod]
		public void HandleArgs_CommandLineArgs_MultiLine()
		{
			//Individual string args
			string[] args = { "[", "KittenService: CamelCaser", ",", "CamelCaser: ", "]" };
			Assert.AreEqual(5, args.Length);

			bool result = Program.HandleArgs(args);
			Assert.AreEqual(true, result);
		}

		[TestMethod]
		public void HandleArgs_CommandLineArgs_MissingFrontSquareBracket()
		{
			//Individual string args
			string[] args = { "KittenService: CamelCaser", ",", "CamelCaser: ", "]" };
			Assert.AreEqual(4, args.Length);

			bool result = Program.HandleArgs(args);
			Assert.AreEqual(false, result);
		}

		[TestMethod]
		public void HandleArgs_CommandLineArgs_MisingBackSquareBracket()
		{
			//Individual string args
			string[] args = { "[", "KittenService: CamelCaser", ",", "CamelCaser: " };
			Assert.AreEqual(4, args.Length);

			bool result = Program.HandleArgs(args);
			Assert.AreEqual(false, result);
		}
		#endregion


		#region ParseArgs
		[TestMethod]
		public void ParseArgsToPackages_FormatOneLine()
		{
			//One line string arg
			string[] args = { "[ KittenService: CamelCaser, CamelCaser:  ]" };
			Assert.AreEqual(1, args.Length);

			List<string[]> result = Program.ParseArgsToPackages(args);
			Assert.IsNotNull(result);
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(2, result[0].Length);
			Assert.AreEqual("KittenService:CamelCaser", result[0][0]);
			Assert.AreEqual("CamelCaser:", result[0][1]);
		}

		[TestMethod]
		public void ParseArgsToPackages_FormatMultiLine()
		{
			//Individual string args
			string[] args = { "[", "KittenService: CamelCaser",",", "CamelCaser: ", "]" };
			Assert.AreEqual(5, args.Length);

			List<string[]> result = Program.ParseArgsToPackages(args);
			Assert.IsNotNull(result);
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(2, result[0].Length);
			Assert.AreEqual("KittenService:CamelCaser", result[0][0]);
			Assert.AreEqual("CamelCaser:", result[0][1]);
		}

		[TestMethod]
		public void ParseArgsToPackages_FormatMultiLineMultiPackages()
		{
			//Individual string args
			string[] args = { "[", "KittenService: CamelCaser", ",", "CamelCaser: ", "]",
									"[" , "KittenService: ",",","Leetmeme: Cyberportal",",", "Cyberportal: Ice",",", "CamelCaser: KittenService",",", "Fraudstream: Leetmeme",",", "Ice: ","]" };
			Assert.AreEqual(18, args.Length);

			List<string[]> result = Program.ParseArgsToPackages(args);
			Assert.IsNotNull(result);
			Assert.AreEqual(2, result.Count);
			Assert.AreEqual(2, result[0].Length);
			Assert.AreEqual("KittenService:CamelCaser", result[0][0]);
			Assert.AreEqual("CamelCaser:", result[0][1]);

			Assert.AreEqual(6, result[1].Length);
			Assert.AreEqual("KittenService:", result[1][0]);
			Assert.AreEqual("Leetmeme:Cyberportal", result[1][1]);
			Assert.AreEqual("Cyberportal:Ice", result[1][2]);
			Assert.AreEqual("CamelCaser:KittenService", result[1][3]);
			Assert.AreEqual("Fraudstream:Leetmeme", result[1][4]);
			Assert.AreEqual("Ice:", result[1][5]);
		}
		#endregion


		#region CreatePackages
		[TestMethod]
		public void CreatePackages_ConvertsFormattedStringsToPackages()
		{
			string[] packages = { "KittenService:CamelCaser", "CamelCaser:" };
			Assert.AreEqual(2, packages.Length);
			try
			{
				List<Packages> result = Program.CreatePackages(packages);
				Assert.IsNotNull(result);
				Assert.AreEqual(2, result.Count);
				Assert.AreEqual("KittenService", result[0].Name);
				Assert.AreEqual(1, result[0].Dependencies.Count);
				Assert.AreEqual("CamelCaser", result[0].Dependencies[0]);
				Assert.AreEqual("CamelCaser", result[1].Name);
				Assert.AreEqual(0, result[1].Dependencies.Count);
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
				Assert.Fail();
			}
		}

		[TestMethod]
		public void CreatePackages_FormmatedInCorrectly()
		{
			string[] packages = { "KittenServiceCamelCaser", "CamelCaser " };
			Assert.AreEqual(2, packages.Length);

			try
			{
				List<Packages> result = Program.CreatePackages(packages);
				//Formatting wasn't done properly
				Assert.Fail();
			}
			catch (Exception ex)
			{
				Assert.IsNotNull(ex);
			}
		}
		#endregion


		#region EvaulatePackages
		[TestMethod]
		public void EvaluatePackages_CreateInstallOrder()
		{
			List<Packages> packages = new List<Packages> { new Packages ("KittenService", "CamelCaser" ),
																		  new Packages ("CamelCaser" ) };
			Assert.AreEqual(2, packages.Count);

			try
			{
				List<Packages> result = Program.EvauluatePackages(packages);
				Assert.IsNotNull(result);
				Assert.AreEqual(1, result.Count);
				Assert.AreEqual("KittenService", result[0].Name);
				Assert.AreEqual(1, result[0].Dependencies.Count);
				Assert.AreEqual("CamelCaser", result[0].Dependencies[0]);
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
				Assert.Fail();
			}
		}

		[TestMethod]
		public void EvaluatePackages_CheckForCycles()
		{
			List<Packages> packages = new List<Packages> { new Packages ( "KittenService","CamelCaser" ),
																		  new Packages ( "CamelCaser", "KittenService" ) };
			Assert.AreEqual(2, packages.Count);

			try
			{
				List<Packages> result = Program.EvauluatePackages(packages);
				//Cycle wasn't detected.
				Assert.Fail();
			}
			catch (Exception ex)
			{
				Assert.IsNotNull(ex);
			}
		}
		#endregion

		#region PrintToConsole
		[TestMethod]
		public void PrintPackagesToConsole_ReverseListOrder()
		{
			List<Packages> packages = new List<Packages> { new Packages ("Leetmeme", "Cyberportal" ),
																		  new Packages ("Cyberportal", "Ice" ) };
			Assert.AreEqual(2, packages.Count);

			List<Packages> result = Program.EvauluatePackages(packages);
			Assert.IsNotNull(result);
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(2, result[0].Dependencies.Count);
			Assert.AreEqual("Cyberportal", result[0].Dependencies[0]);
			Assert.AreEqual("Ice", result[0].Dependencies[1]);

			Program.PrintPackagesToConsole(result);
			Assert.IsNotNull(result);
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(2, result[0].Dependencies.Count);
			Assert.AreEqual("Ice", result[0].Dependencies[0]);
			Assert.AreEqual("Cyberportal", result[0].Dependencies[1]);
		}
		#endregion
	}
}
