﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PluralSightPackageInstaller;

namespace PluralSightPackageInstallerTests
{
	[TestClass]
	public class PackagesUnitTests
	{
		[TestMethod]
		public void PropertyInitilizing()
		{
			var package = new Packages();
			Assert.IsNotNull(package);
			Assert.IsNull(package.Name);
			Assert.IsNotNull(package.Dependencies);
			Assert.AreEqual(0, package.Dependencies.Count);

			package.Name = "KittenService";
			package.Dependencies.Add("CamelCaser");
			Assert.IsNotNull(package);
			Assert.IsNotNull(package.Name);
			Assert.AreEqual(1, package.Dependencies.Count);
			Assert.AreEqual("CamelCaser", package.Dependencies[0]);
		}

		#region Constructor
		[TestMethod]
		public void Constructor_NoParams()
		{
			var package = new Packages();
			Assert.IsNotNull(package);
			Assert.IsNull(package.Name);
			Assert.IsNotNull(package.Dependencies);
			Assert.AreEqual(0, package.Dependencies.Count);
		}

		[TestMethod]
		public void Constructor_OnlyNameParam()
		{
			var package = new Packages("KittenService");
			Assert.IsNotNull(package);
			Assert.AreEqual("KittenService", package.Name);
			Assert.IsNotNull(package.Dependencies);
			Assert.AreEqual(0, package.Dependencies.Count);
		}

		[TestMethod]
		public void Constructor_NameNullParams()
		{
			var package = new Packages("KittenService", null);
			Assert.IsNotNull(package);
			Assert.AreEqual("KittenService", package.Name);
			Assert.IsNotNull(package.Dependencies);
			Assert.AreEqual(0, package.Dependencies.Count);
		}

		[TestMethod]
		public void Constructor_OnlyDependencyParam()
		{
			var package = new Packages(null, "CamelCaser");
			Assert.IsNotNull(package);
			Assert.IsNull(package.Name);
			Assert.IsNotNull(package.Dependencies);
			Assert.AreEqual(1, package.Dependencies.Count);
			Assert.AreEqual("CamelCaser", package.Dependencies[0]);
		}

		[TestMethod]
		public void Constructor_NullParams()
		{
			var package = new Packages(null, null);
			Assert.IsNotNull(package);
			Assert.IsNull(package.Name);
			Assert.IsNotNull(package.Dependencies);
			Assert.AreEqual(0, package.Dependencies.Count);
		}
		#endregion
	}
}
