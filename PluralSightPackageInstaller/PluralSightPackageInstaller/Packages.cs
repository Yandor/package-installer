﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluralSightPackageInstaller
{
	public class Packages
	{
		public string Name { get; set; }
		public List<string> Dependencies { get; private set; }

		public Packages(string name = null, string dependency = null)
		{
			Name = name;
			Dependencies = dependency != null ? new List<string> { dependency } : new List<string>();
		}
	}
}
