﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluralSightPackageInstaller
{
	public static class Program
	{
		/// <summary>
		/// Main entry point for CommandLine project, expecting either a string with square brackets at start and end "[ KittenService: CamelCaser, CamelCaser: ]".
		/// Or expecting individual strings aka "[" "KittenService: CamelCaser", "CamelCaser: " "]"
		/// </summary>
		/// <param name="args">an array of strings containing installer package names with their dependencies</param>
		public static void Main(string[] args)
		{
			// Check to see if we have recieved expected command line args.
			if (HandleArgs(args))
			{
				try
				{
					//Format command line args into packages with dependecies
					var stringPackageGroups = ParseArgsToPackages(args);
					

					foreach (var stringPackageGroup in stringPackageGroups)
					{
						//Create packages from formatted string array
						var packageGroup = CreatePackages(stringPackageGroup);
						//Evauluate packages for proper install order.
						var printablePackages = EvauluatePackages(packageGroup);
						//Print out packages in proper install order.
						PrintPackagesToConsole(printablePackages);
					}
				}
				catch(Exception ex)
				{
					Console.Error.WriteLine(ex.Message);
				}
			}
		}

		/// <summary>
		/// Takes command line args and determines if they are in a valid state to use.
		/// </summary>
		/// <param name="args">an array of strings containing installer package names with their dependencies</param>
		/// <returns>a bool determining if args are what we expect them to be.</returns>
		public static bool HandleArgs(string[] args)
		{
			//Check to make sure we got something in the command line
			if (args.Length <= 0)
			{
				Console.Error.WriteLine("Error - No command Line arguments given.");
				return false;
			}

			//Making sure the commandline arg is formated properly
			string input = string.Join(" ", args);
			if (input.Count(arg => arg == '[') != input.Count(arg => arg == ']'))
			{
				Console.Error.WriteLine("Error - Installer input isn't formatted properly.");
				return false;
			}

			return true;
		}

		/// <summary>
		/// Formats commandline args into Package:Dependency setup.
		/// </summary>
		/// <param name="args">an array of strings containing installer package names with their dependencies</param>
		/// <returns>an array of strings, that are formatted - Package:Dependency</returns>
		public static List<string[]> ParseArgsToPackages(string[] args)
		{
			//Incase the arguments don't come in as a whole string, but in individual parts aka: "[" "KittenService: CamelCaser", "CamelCaser: " "]"
			//This wont effect the strings if arguments are one string aka: "[ KittenService: CamelCaser, CamelCaser: ]"
			string input = string.Join(" ", args);

			//Strip down the string of non essential characters
			input = input.Replace("\r", "").Replace("\n", "").Replace("'", "").Replace("\"", "").Replace(" ", "").Trim();

			var packageGroups = input.Split(']');

			var packageList = new List<string[]>();
			foreach (var packageGroup in packageGroups)
			{
				if (packageGroup != "")
				{
					var temp = packageGroup.Replace("[", "").Replace("]", "").Trim();
					packageList.Add(temp.Split(','));
				}
			}

			return packageList;
		}

		/// <summary>
		/// Takes formatted array of strings and creates Packages out of them
		/// </summary>
		/// <param name="packages">an array of strings formatted setup - Package:Dependency</param>
		/// <returns>A List of packages ready to be evauluated to find install order</returns>
		public static List<Packages> CreatePackages(string[] packages)
		{
			List<Packages> packageList = new List<Packages>();

			//cycle through and create the packages with any dependances
			foreach (var piece in packages)
			{
				if (piece.Contains(":"))
				{
					//Split Packages from Dependances
					var index = piece.IndexOf(":", StringComparison.Ordinal);
					var name = piece.Substring(0, index);
					var dependency = index + 1 < piece.Length ? piece.Substring(index + 1) : null;

					var newPackage = new Packages(name, dependency);

					packageList.Add(newPackage);
				}
				else
				{
					throw new Exception("Error - Package input has no ':' found in it: " + piece + ".");
				}
			}
			return packageList;
		}

		/// <summary>
		/// Takes Packages and evaulates them to find the proper install order. Also checks for cycles.
		/// </summary>
		/// <param name="packges">A List of packages</param>
		/// <returns>A a checked List of packages, that contain no cycles</returns>
		public static List<Packages> EvauluatePackages(List<Packages> packages)
		{
			List<Packages> newPackages = new List<Packages>();

			foreach (var package in packages)
			{
				//Check to see if Package has already been evaluated
				if (newPackages.Any(p => p.Name == package.Name))
				{
					//Already evaluated a package with this name...
					Console.Error.WriteLine("Warning: Package name has already been evaluated before: {0}", package.Name);
					continue;
				}

				//Check to see if Package is a dependency of another Package
				var tempPackage = newPackages.FirstOrDefault(p => p.Dependencies.Any(d => d == package.Name) == true);
				if (tempPackage != null)
				{
					//Check to see if Package's dependencies are already accounted for (Cycle)
					if (package.Dependencies.Any(d => d == tempPackage.Name))
					{
						throw new Exception("Error - Found a cycle in dependencies");
					}
					//Add package's dependencies to higher order package.
					else
					{
						tempPackage.Dependencies.AddRange(package.Dependencies);
					}
				}
				//Package doesn't depend on anything else (so far) add to list
				else
				{
					newPackages.Add(package);
				}
			}
			return newPackages;
		}

		/// <summary>
		/// Reverse package dependency, and printout to console the install order.
		/// </summary>
		/// <param name="packages">List of packages already been validated and ready to use</param>
		public static void PrintPackagesToConsole(List<Packages> packages)
		{
			List<string> printList = new List<string>();

			//Flatten down the list and remove duplicates.
			foreach (var package in packages)
			{
				package.Dependencies.Reverse();

				//Look through dependencies and added ones that haven't already been accounted for by another package.
				foreach (var dependency in package.Dependencies)
				{
					//Check to see if dependency was already added from another package
					if (printList.Any(p => p == dependency) == false)
					{
						printList.Add(dependency);
					}
				}
				printList.Add(package.Name);
			}

			//Printout valid dependencies in order of importance.
			Console.WriteLine(string.Join(", ", printList));
		}
	}
}
